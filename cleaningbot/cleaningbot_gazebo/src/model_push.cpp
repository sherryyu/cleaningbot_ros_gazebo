#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <stdio.h>
#include <ros/ros.h>
#include "cleaningbot_control/JointPos.h"

namespace gazebo
{
class ModelPush : public ModelPlugin
{

public:
	physics::JointPtr base_revolute_joint;
	physics::JointPtr arm1_joint;
	physics::JointPtr arm2_joint;
	physics::JointController* _joint_controller;
	ros::NodeHandle rosnode;
	ros::Subscriber joint_controller_sub;
	float arm1_joint_target_pos, arm2_joint_target_pos, base_revolute_joint_target_pos;

public: void ensureJointExists(physics::JointPtr joint, std::string name)
{
//	if(joint == NULL)
//	{
//		joint = model->GetWorld()->GetPhysicsEngine()->CreateJoint("revolute", model);
//		joint->SetName(name.c_str());
//	}
}

public: void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
{
	// Store the pointer to the model
	this->model = _parent;
	int argc = 0;
	char** argv = NULL;

	joint_max_effort = 10;	// ********test**********


	ros::init(argc, argv, "ROS joint plugin");
	std::string model_name = this->model->GetName();
	//std::cout<<"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ "<<model_name<<std::endl;
	std::stringstream joint_controller_topic_name;
	joint_controller_topic_name<<"/"<<model_name<<"/joint_controller";
	joint_controller_sub = rosnode.subscribe(joint_controller_topic_name.str().c_str(), 10, &ModelPush::JCCallback, this);



	this->arm1_joint = this->model->GetJoint("sherripper::arm1_joint");
	this->arm2_joint = this->model->GetJoint("sherripper::arm2_joint");
	this->base_revolute_joint = this->model->GetJoint("sherripper::base_revolute_joint");
	_joint_controller = new physics::JointController(this->model);

	_joint_controller->AddJoint(arm1_joint);
	_joint_controller->AddJoint(arm2_joint);
	_joint_controller->AddJoint(base_revolute_joint);

	wallModel = this->model->GetWorld()->GetModel("grey_wall");
	suctionLink = this->model->GetLink("base");
//	suctionLink = this->model->GetLink("suction_cup");
	wallLink = wallModel->GetLink("link");
	dynamic_joint = model->GetWorld()->GetPhysicsEngine()->CreateJoint("revolute", model);
	dynamic_joint->SetName("dynamic_joint");
	dynamic_joint_connected = false;

	gripperLink = model->GetLink("sherripper::arm2");
//	gripperLink = model->GetLink("base");
	gripper_joint = model->GetWorld()->GetPhysicsEngine()->CreateJoint("revolute", model);
	gripper_joint->SetName("gripper_joint");
	gripper_joint_connected = false;

	_joint_controller->SetJointPosition(arm1_joint,0);
	_joint_controller->SetJointPosition(arm2_joint,0);
	_joint_controller->SetJointPosition(base_revolute_joint,3.14159/2.0);

	// Listen to the update event. This event is broadcast every
	// simulation iteration.
	this->updateConnection = event::Events::ConnectWorldUpdateBegin(
			boost::bind(&ModelPush::OnUpdate, this, _1));

}

void JCCallback(const cleaningbot_control::JointPos::ConstPtr& msg)
{
	// you update the position of where you want the joint to move
	std::string target_joint_name = msg->joint_name;
	if(target_joint_name.compare("arm1_joint") == 0){
		arm1_joint_target_pos = msg->position;
	}else if(target_joint_name.compare("arm2_joint") == 0){
		arm2_joint_target_pos = msg->position;
	}else if(target_joint_name.compare("base_revolute_joint") == 0){
		base_revolute_joint_target_pos = msg->position;
	}else if(target_joint_name.compare("dynamic_joint") == 0){
		// create a fixed joint so that robot stays in one place
		// TODO: it only works on the wall so far, proper way to do it is creating a contact sensor and get the other link's name
		if(msg->position == 0){
			//  				// create joint
			ensureJointExists(dynamic_joint, "dynamic_joint");
			if(!dynamic_joint_connected)
			{
				dynamic_joint->Attach(wallLink,suctionLink);
				dynamic_joint_connected = true;
			}
			dynamic_joint->Load(wallLink, suctionLink,math::Pose());
			dynamic_joint->SetAxis(0, gazebo::math::Vector3(0,1,0));

		}else{
			// detach joint
			dynamic_joint->Detach();
			dynamic_joint_connected = false;
		}
	}else{
		std::string grip("_grip::collision");
		if(target_joint_name.find(grip) != std::string::npos){
			// create a fixed joint so that gripper can grab onto another robot
			if(msg->position == 0){
				std::string gripeeModelName = target_joint_name.substr(0,12);
				size_t pos = 14;
				std::string gripeeLinkName = target_joint_name.substr(14,9);
				gripeeModel = this->model->GetWorld()->GetModel(gripeeModelName.c_str());
				gripeeLink = gripeeModel->GetLink(gripeeLinkName.c_str());

				// create joint
				ensureJointExists(gripper_joint, "gripper_joint");
				if(!gripper_joint_connected)
				{
					gripper_joint->Attach(gripeeLink,gripperLink);
					gripper_joint_connected = true;
//					printf("Attached: target_joint_name %s\n", target_joint_name.c_str());
				}

				gripper_joint->Load(gripeeLink,gripperLink,math::Pose());
				gripper_joint->SetAxis(0, gazebo::math::Vector3(0,1,0));
				gazebo::math::Angle upper(0.0);
				gripper_joint->SetHighStop(0,upper);
				gripper_joint->SetLowStop(0,upper);
			}
		}else if(target_joint_name.compare("detach_gripper") == 0){
			// detach joint
			gripper_joint->Detach();
//			printf("detaching \n");
			gripper_joint_connected = false;
		}
	}
}

// Called by the world update start event
public: void OnUpdate(const common::UpdateInfo & /*_info*/)
{
	// Apply a small force to each link in the model
	const physics::Link_V links = this->model->GetLinks();
	//physics::LinkPtr suctioncup = this->model->GetLink(std::string("base"));
	physics::LinkPtr suctioncup = this->model->GetLink(std::string("suction_cup"));

	const math::Pose suctionCupPose = suctioncup->GetWorldPose();
	math::Vector3 forceVec(2.234,2.234,2.234);
	forceVec *= suctionCupPose.rot.RotateVector(math::Vector3(0,0,-1));
	// std::cout<<"forceVec "<< forceVec<<std::endl;
	suctioncup->AddForce(forceVec);

	// update target joint positions
	_joint_controller->SetJointPosition(arm1_joint,arm1_joint_target_pos);
	_joint_controller->SetJointPosition(arm2_joint,arm2_joint_target_pos);
	_joint_controller->SetJointPosition(base_revolute_joint,base_revolute_joint_target_pos);
	this->_joint_controller->Update();
}

// Pointer to the model
private: physics::ModelPtr model;
common::Time prevUpdateTime;
float joint_max_effort;
gazebo::physics::JointPtr dynamic_joint;
gazebo::physics::JointPtr gripper_joint;
gazebo::physics::LinkPtr gripeeLink;
gazebo::physics::LinkPtr gripperLink;
gazebo::physics::ModelPtr wallModel;
gazebo::physics::ModelPtr gripeeModel;
gazebo::physics::LinkPtr suctionLink;
gazebo::physics::LinkPtr wallLink;
bool dynamic_joint_connected;
bool gripper_joint_connected;

// Pointer to the update event connection
private: event::ConnectionPtr updateConnection;
};

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(ModelPush)
}
