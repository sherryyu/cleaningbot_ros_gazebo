#include <gazebo/common/Plugin.hh>
#include <ros/ros.h>
#include "gazebo/msgs/msgs.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/transport/transport.hh"
#include <sdf/sdf.hh>
#include "gazebo/gazebo.hh"

namespace gazebo
{
class WorldPluginTutorial : public WorldPlugin
{
public:
  WorldPluginTutorial() : WorldPlugin()
  {
  }

  void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
  {
    // Make sure the ROS node for Gazebo has already been initialized                                                                                    
    if (!ros::isInitialized())
    {
      ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
        << "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
      return;
    }

    // Create a new transport node
      transport::NodePtr node(new transport::Node());

      // Initialize the node with the world name
      node->Init(_world->GetName());

      // Create a publisher on the ~/physics topic
      transport::PublisherPtr physicsPub =
        node->Advertise<msgs::Physics>("~/physics");

      msgs::Physics physicsMsg;
      physicsMsg.set_type(msgs::Physics::ODE);

      // Set the step time
      physicsMsg.set_max_step_size(0.01);

	// increase iterations
	physicsMsg.set_iters(100);

	physicsMsg.set_real_time_update_rate(900.0);

      // Change gravity
    msgs::Set(physicsMsg.mutable_gravity(), math::Vector3(0.0, 0.0, 0.0));
      physicsPub->Publish(physicsMsg);
  }

};
GZ_REGISTER_WORLD_PLUGIN(WorldPluginTutorial)
}
