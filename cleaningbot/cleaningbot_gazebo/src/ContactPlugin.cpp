#ifndef _GAZEBO_CONTACT_PLUGIN_HH_
#define _GAZEBO_CONTACT_PLUGIN_HH_

#include <string.h>
#include <string>

#include <gazebo/gazebo.hh>
#include <gazebo/sensors/sensors.hh>
#include <ros/ros.h>
#include "cleaningbot_control/ContactReading.h"

using namespace gazebo;

namespace gazebo
{
/// \brief An example plugin for a contact sensor.
class ContactPlugin : public SensorPlugin
{
	/// \brief Constructor.
public: ContactPlugin();

ros::NodeHandle rosnode;
ros::Publisher contact_pub_;
cleaningbot_control::ContactReading contactReadingMsg;
char * modelName;
bool lastStatus;

/// \brief Destructor.
public: virtual ~ContactPlugin();

/// \brief Load the sensor plugin.
/// \param[in] _sensor Pointer to the sensor that loaded this plugin.
/// \param[in] _sdf SDF element that describes the plugin.
public: virtual void Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf)
{
	//printf("ContactPlugin::Load(): load plugin\n");
	modelName = strtok((char *)_sensor->GetParentName().c_str(), ":");
	int argc = 0;
	char** argv = NULL;
	//printf("Sensor's parent: %s\n", modelName);
	ros::init(argc, argv, "ROS contact sensor plugin");
	std::stringstream contact_topic_name;
	contact_topic_name<<"/"<<modelName<<"/contact_reading";
	contact_pub_ = rosnode.advertise<cleaningbot_control::ContactReading>(contact_topic_name.str().c_str(), 1000);

	contactReadingMsg.robotName = modelName;
	contactReadingMsg.inContact = false;
	lastStatus = false;
	// Get the parent sensor.
	this->parentSensor =
			boost::dynamic_pointer_cast<sensors::ContactSensor>(_sensor);

	// Make sure the parent sensor is valid.
	if (!this->parentSensor)
	{
		gzerr << "ContactPlugin requires a ContactSensor.\n";
		return;
	}

	// Connect to the sensor update event.
	this->updateConnection = this->parentSensor->ConnectUpdated(
			boost::bind(&ContactPlugin::OnUpdate, this));

	// Make sure the parent sensor is active.
	this->parentSensor->SetActive(true);
}


/// \brief Callback that receives the contact sensor's update signal.
private: virtual void OnUpdate()
{
	// Get all the contacts.
	msgs::Contacts contacts;
	contacts = this->parentSensor->GetContacts();

	contactReadingMsg.robotName = modelName;

	if(contacts.contact_size()>0){
		contactReadingMsg.inContact = true;

		// find the robot that the gripper is connected with and publish the link
		std::string grip("_grip::collision");
		if(contacts.contact(0).collision1().find(grip) != std::string::npos){
			contactReadingMsg.robotName = contacts.contact(0).collision1();
		}else if(contacts.contact(0).collision2().find(grip) != std::string::npos){
			contactReadingMsg.robotName = contacts.contact(0).collision2();
		}

	}else{
		contactReadingMsg.inContact = false;
	}

	if(contactReadingMsg.inContact != lastStatus){
		contact_pub_.publish(contactReadingMsg);
	}
	lastStatus = contactReadingMsg.inContact;

//	for (unsigned int i = 0; i < contacts.contact_size(); ++i)
//	{
//
//		std::cout << "Collision between[" << contacts.contact(i).collision1()
//            				  << "] and [" << contacts.contact(i).collision2() << "]\n";
//
//		//		for (unsigned int j = 0; j < contacts.contact(i).position_size(); ++j)
//		//		{
//		//			std::cout << j << "  Position:"
//		//					<< contacts.contact(i).position(j).x() << " "
//		//					<< contacts.contact(i).position(j).y() << " "
//		//					<< contacts.contact(i).position(j).z() << "\n";
//		//			std::cout << "   Normal:"
//		//					<< contacts.contact(i).normal(j).x() << " "
//		//					<< contacts.contact(i).normal(j).y() << " "
//		//					<< contacts.contact(i).normal(j).z() << "\n";
//		//			std::cout << "   Depth:" << contacts.contact(i).depth(j) << "\n";
//		//		}
//
//	}

}

/// \brief Pointer to the contact sensor
private: sensors::ContactSensorPtr parentSensor;

/// \brief Connection that maintains a link between the contact sensor's
/// updated signal and the OnUpdate callback.
private: event::ConnectionPtr updateConnection;
};


/////////////////////////////////////////////////
ContactPlugin::ContactPlugin() : SensorPlugin()
{
	//printf("ContactPlugin(): starting up\n");
	lastStatus = false;
	modelName = NULL;
}

/////////////////////////////////////////////////
ContactPlugin::~ContactPlugin()
{
}
}

GZ_REGISTER_SENSOR_PLUGIN(ContactPlugin)

#endif
