#include "robotdriver.h"

const char* broadcast_channel_name = "cleaning_broadcast";


float Clampit(float it, float min, float max){
	if(max<min){
		std::cout<<"WTF?!!"<<std::endl;
	}

	if(it>max){
		return max;
	}else if(it < min){
		return min;
	}

	return it;
}

int Clampit(int it, int min, int max){
	if(max<min){
		std::cout<<"WTF?!!"<<std::endl;
	}

	if(it>max){
		return max;
	}else if(it < min){
		return min;
	}

	return it;
}

//! ROS node initialization
RobotDriver::RobotDriver(ros::NodeHandle &n, int robotIndex)
{
	n_ = n;
	//set up the publisher for the cmd_vel topic
	std::stringstream buffer_cmd, buffer_name,laser_topic,joint_name,joint_controller_topic_name,contact_topic;
	robot_index = robotIndex;
	total_robot_number = 0;

	buffer_cmd<<"/cleaningbot"<<robotIndex<<"/cmd_vel";
	buffer_name<<"cleaningbot"<<robotIndex;
	laser_topic<<"/cleaningbot"<<robotIndex<<"/laser";
	contact_topic<<"/cleaningbot"<<robotIndex<<"/contact_reading";
	joint_name<<"sherripper::arm1_joint";

	gripperContact = false;

	joint_controller_topic_name<<"/cleaningbot"<<robotIndex<<"/joint_controller";

	// send command to actuators
	cmd_vel_pub_ = n_.advertise<geometry_msgs::Twist>(buffer_cmd.str().c_str(), 1);

	// advertise and subscribe broadcast message
	RS_broadcast_channel_pub_ = n_.advertise<cleaningbot_control::RSPacket>(broadcast_channel_name, 1000);
	RS_broadcast_channel_sub_ = n_.subscribe(broadcast_channel_name, 1000, &RobotDriver::RSCallback, this);

	// gripper joint PID controller
	joint_controller_pub_ = n_.advertise<cleaningbot_control::JointPos>(joint_controller_topic_name.str().c_str(), 1000);

	// get robot state data
	model_state_client_ = n_.serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state");
	sensor_laser_data_sub_ = n_.subscribe(laser_topic.str().c_str(), 1000, &RobotDriver::laserCallback, this);
	contact_reading_sub_ = n_.subscribe(contact_topic.str().c_str(), 1000, &RobotDriver::contactCallback, this);
	gms.request.model_name = buffer_name.str().c_str();
	gjp.request.joint_name = joint_name.str().c_str();

	//commPacket.robot_id = robotIndex;
	double q = 0.0;
	current_orientation_quar.Set(q,q,q,q);

	robotState.position = gazebo::math::Vector3(0.0,0.0,0.0);
	initial_orientation_vector = gazebo::math::Vector3(1.0,0.0,0.0); //initialized to be roll 0 pitch 0 yaw 0
	target_orientation = gazebo::math::Vector3(-1.0,0.0,0.0);

	next_target = false;

	arm1_joint_name = "arm1_joint";
	arm2_joint_name = "arm2_joint";
	base_revolute_joint_name = "base_revolute_joint";
	gripee_name = "gripee";

	arm1_joint_position = 0.0;
	arm2_joint_position = 0.0;
	base_revolute_joint_position = 0.0;

	trapped = false;
	trapCount = 0;
	robot_status = INIT;

	// define grey wall normal vector
	grey_wall_normal = gazebo::math::Vector3(0,-1.0,0);

	if(robot_index == 1){
		cleaning_init_step = GO_TO_STARTING_POS;
	}else{
		cleaning_init_step = WAITING;
	}

	cleaning_init_step = GO_TO_STARTING_POS;

	targetInPosition = false;
	organismReady = false;
	targetInOrganism = false;
	detach = false;
	horizontalMove = true;
	horizontalMoveDirection = 1.0;

	squad_ID = 0;
	squad_numbers_required.push_back(4);

	way_point = gazebo::math::Vector3(0.0,0.0,0.0);

	arm1_angle = 0.0;
	base_revolute_angle = 0.0;
	gripperTouched = false;
	for (int i = 0; i<total_robot_number;i++){
		readyToStartCleaning.push_back(0);
	}
}

int RobotDriver::runLoop(){

	/* 			Initializations for control loops			*/

	// find neighbours
	neighbours.clear();
	updateSensorData();

	// select a state




	// go to starting position
	switch(robot_status){
	case IDLE:{

		break;
	}
	case INIT:{
		// go to an intermediate position
		gazebo::math::Vector3 starting_position(robotState.position.x,robotState.position.y ,2.9 - robot_index*GRIPPER_CONNECTION_DISTANCE);
		//		gazebo::math::Vector3 target_orientation(-1.0,robotState.orientation.y,0.0);
		if(setSpeedFromWorldPosition(starting_position,target_orientation,grey_wall_normal)){
			robot_status = CLEANING;
		}

		// determine how many robots are needed in a squad


		break;
	}
	case OBSTACLE:{

		break;
	}
	case RECHARGING:{


		break;
	}
	case FAULT:{


		break;
	}
	case RESCUE:{


		break;
	}
	case CONNECTIVITY:{


		break;
	}
	case EXTRA_DIRTY:{

		break;
	}
	case CLEANING:{

		cleaningUpdate();

		break;
	}
	case RECRUITING:{


		break;
	}
	default:{

	}
	}


	/**                                                              **/
	/************************ Finishing Touches ***********************/
	/**                                                              **/
	// cancel the turbulence in simulator
	if(base_cmd.linear.x == 0.0){
		setGripperJointsPosition("dynamic_joint", 0);
	}else{
		setGripperJointsPosition("dynamic_joint", 1);
	}
	// send actuator control commands
	cmd_vel_pub_.publish(base_cmd);
	// send communication packets
	//	sendPackets(0.0,(int)robot_status, 0);

	return 0;
}

void RobotDriver::updateSensorData(){
	// display the robot position
	model_state_client_.call(gms);
	float x,y,z;

	// update GPS data
	x = gms.response.pose.position.x;
	y = gms.response.pose.position.y;
	z = gms.response.pose.position.z;
	robotState.Vx = fabs(sqrtf(gms.response.twist.linear.x*gms.response.twist.linear.x+gms.response.twist.linear.y*gms.response.twist.linear.y+gms.response.twist.linear.z*gms.response.twist.linear.z));
	robotState.Wz = fabs(sqrtf(gms.response.twist.angular.x*gms.response.twist.angular.x+gms.response.twist.angular.y*gms.response.twist.angular.y+gms.response.twist.angular.z*gms.response.twist.angular.z));
	robotState.position = gazebo::math::Vector3(x,y,z);
	//gms.response.pose.orientation
	current_orientation_quar.Set(gms.response.pose.orientation.w,gms.response.pose.orientation.x,gms.response.pose.orientation.y,gms.response.pose.orientation.z);
	robotState.orientation = current_orientation_quar.RotateVector(initial_orientation_vector);
	robotState.orientation = robotState.orientation.Normalize();

	// update IR sensor data to detect neighboring robots
	gazebo_msgs::GetModelState gms_neighbours;
	for(int i = 1; i<= total_robot_number; i++){
		if(i != robot_index){
			std::stringstream buffer_neighbourname;
			buffer_neighbourname<<"cleaningbot"<<i;
			gms_neighbours.request.model_name = buffer_neighbourname.str().c_str();
			model_state_client_.call(gms_neighbours);
			gazebo::math::Vector3 otherRobot_position(gms_neighbours.response.pose.position.x, gms_neighbours.response.pose.position.y, gms_neighbours.response.pose.position.z);
			gazebo::math::Vector3 my_position(x,y,z);
			gazebo::math::Vector3 dist = otherRobot_position - my_position;

			if(dist.GetLength() <= SENSING_RANGE){
				neighbours_map neighbour;
				neighbour.neighbour_index = i;
				neighbour.relative_position = current_orientation_quar.RotateVector(dist);
				neighbours.push_back(neighbour);
			}
		}
	}
}


void RobotDriver::sendPackets(float hormone_levels, int robot_status, int data){

	//	printf("sendPackets(): robot %i sending data %i\n", robot_index, data);

	RS_send_packet.robot_id = robot_index;
	RS_send_packet.hormone_levels.clear();
	RS_send_packet.hormone_levels.push_back(hormone_levels);
	RS_send_packet.robot_status = robot_status;
	RS_send_packet.position.clear();
	RS_send_packet.position.push_back(robotState.position.x);
	RS_send_packet.position.push_back(robotState.position.y);
	RS_send_packet.position.push_back(robotState.position.z);
	RS_send_packet.orientation.clear();
	RS_send_packet.orientation.push_back(robotState.orientation.x);
	RS_send_packet.orientation.push_back(robotState.orientation.y);
	RS_send_packet.orientation.push_back(robotState.orientation.z);
	RS_send_packet.data = data;
	RS_broadcast_channel_pub_.publish(RS_send_packet);
}


void RobotDriver::setGripperJointsPosition(std::string target_joint_name, float target_pos){
	JointControllerMsg.joint_name = target_joint_name;
	JointControllerMsg.position = target_pos;
	joint_controller_pub_.publish(JointControllerMsg);
}


int RobotDriver::setSpeedFromWorldPosition(gazebo::math::Vector3& target_pos, gazebo::math::Vector3& target_orientation, gazebo::math::Vector3& normal){
	// calculate heading vector in 3D
	gazebo::math::Vector3 heading, projected_heading;
	heading = target_pos - robotState.position;

	// calculate target on the plane the robot is on, so that the heading vector is calibrated
	float n_abs = heading.Dot(normal.Normalize());
	gazebo::math::Vector3 n = n_abs*normal.Normalize();
	projected_heading = heading - n;

	//	// calculate error on the plane
	//float norm_length = projected_heading.Dot(normal.Normalize());
	float error = projected_heading.GetLength();

	// get robot y axis direction
	gazebo::math::Vector3 robot_y_direction(current_orientation_quar.RotateVector(gazebo::math::Vector3(0.0,1.0,0.0)));

	// find the angle between calibrated heading vector and robot x axis
	float theta_x = projected_heading.Dot(robotState.orientation);
	if(projected_heading.GetLength()<=0.00001){
		theta_x = 0.0;
	}else{
		theta_x = theta_x/(projected_heading.GetLength()*robotState.orientation.GetLength());
		theta_x = acos(theta_x);
	}

	// fidn the angle between calibrated heading vector and robot y axis
	float theta_y = projected_heading.Dot(robot_y_direction);
	if(projected_heading.GetLength()<=0.00001){
		theta_y = 0.0;
	}else{
		theta_y = theta_y/(projected_heading.GetLength()*robot_y_direction.GetLength());
		theta_y = acos(theta_y);
	}
	// get heading angle
	if(theta_y>PI/2.0){
		theta_x = -theta_x;
	}

	// calculate error in robot x direction and y direction
	float error_x = projected_heading.GetLength()*cos(theta_x);

	base_cmd.linear.x = ROBOT_MAX_SPEED < fabs(error_x)*0.2? copysign(ROBOT_MAX_SPEED,error_x) : error_x*0.2;
	base_cmd.angular.z = ROBOT_MAX_ANGULAR_SPEED < fabs(theta_x*0.5)? copysign(ROBOT_MAX_ANGULAR_SPEED,theta_x) : theta_x*0.5;


	// **************check if the robot is trapped**************
	if(trapped||checkForTrapped(200,error, base_cmd.linear.x,base_cmd.angular.z)){

		trapped = true;
		float obsX,obsY;
		calculateObstacleVectorFromLaser(obsX,obsY);
		//		std::cout << "obsX = "<<obsX<<"obsY = "<<obsY<<std::endl;
		float obs_theta = atan2(obsY,obsX);

		// calculate and clamp heading vector in robot coordinates
		if(fabs(error)> 2.0){
			error = 2.0;
		}
		float heading_localX = error*cos(theta_x);
		float heading_localY = error*sin(theta_x);
		//		std::cout<<"obs_theta = "<< obs_theta;

		//calculate obs_theta normal that's in robot orientation
		float obs_normal;
		obs_normal = obs_theta - (float)PI/2.0;
		if(fabs(obs_normal)>(float)PI/2.0){
			obs_normal += (float)PI;
		}

		float wallParallelLength = 2.0;
		float wallParallelAngle = obs_normal;

		// calculating LJ is not actually necessary, but I'll put it here just in case;
		float LJForceLength = GeneralizedLennardJones(sqrt(obsY*obsY+obsX*obsX));
		float LJForceAngle = obs_theta;

		// calculate wall following vector
		float wallFollowingX = wallParallelLength*cos(wallParallelAngle)+LJForceLength*cos(LJForceAngle);
		float wallFollowingY = wallParallelLength*sin(wallParallelAngle)+LJForceLength*sin(LJForceAngle);
		float wallFollwingAngle = atan2(wallFollowingY,wallFollowingX);

		base_cmd.linear.x = ROBOT_MAX_SPEED < fabs(wallFollowingX)*0.02? copysign(ROBOT_MAX_SPEED,wallFollowingX) : wallFollowingX*0.02;
		base_cmd.angular.z = ROBOT_MAX_ANGULAR_SPEED < fabs(wallFollwingAngle*0.2)? copysign(ROBOT_MAX_ANGULAR_SPEED,wallFollwingAngle) : wallFollwingAngle*0.2;

		//check for free
		if(fabs(obs_theta-theta_x)>(0.5*PI)){
			trapped = false;
			trapCount --;
		}
	}

	float orientation_error_x = robotState.orientation.Dot(target_orientation);
	orientation_error_x = orientation_error_x/(robotState.orientation.GetLength()*target_orientation.GetLength());
	orientation_error_x = acos(orientation_error_x);


	float ori_err_y = robot_y_direction.Dot(target_orientation);
	ori_err_y = ori_err_y/(target_orientation.GetLength()*robot_y_direction.GetLength());
	ori_err_y = acos(ori_err_y);
	if(ori_err_y>PI/2.0){
		orientation_error_x = -orientation_error_x;
	}

	if(fabs(error) <= POSITION_ERROR_THRESHOLD){
		base_cmd.linear.x = 0.0;

		// set angular speed
		base_cmd.angular.z = 0.5;

		if(fabs(orientation_error_x)<0.5){
			base_cmd.angular.z = orientation_error_x*0.2;
		}
		else if(fabs(orientation_error_x)<1.5){
			base_cmd.angular.z = 0.4;
		}

		base_cmd.angular.z = copysign(base_cmd.angular.z,orientation_error_x);
//		printf("angular z %f", base_cmd.angular.z);
		if(fabs(orientation_error_x)<PI*0.5/180.0){
			base_cmd.angular.z = 0.0;
			return 1;
		}
	}
//	printf("angular z %f", base_cmd.angular.z);
	return 0;
}

void RobotDriver::laserCallback(const sensor_msgs::LaserScan::ConstPtr& msg){
	laserData.clear();
	for (int i = 0; i<msg->ranges.size(); i++){
		laserData.push_back(msg->ranges[i]);
		//		std::cout<<" val = "<< msg->ranges[i];
	}
	//	std::cout<<" "<<std::endl;
}

void RobotDriver::contactCallback(const cleaningbot_control::ContactReading::ConstPtr& contactMsg){
	gripperContact = contactMsg->inContact;
	gripee_name = contactMsg->robotName;
	if(gripperContact&&(!detach)){
		gripperTouched = true;
		setGripperJointsPosition(contactMsg->robotName, 0); 	// 0 for creating joint
	}
}


void RobotDriver::RSCallback(const cleaningbot_control::RSPacket::ConstPtr& packet){
	int robotIndex_rx = packet->robot_id;
	if(robotIndex_rx != robot_index){
		switch(robot_status){
		case CLEANING:{

			switch(packet->data){
			case GO_TO_STARTING_POS:{
				if(robotIndex_rx == (robot_index +1)){
					targetInPosition = true;
				}
				//				else if(robotIndex_rx == (robot_index -1)){
				//					cleaning_init_step = GO_TO_STARTING_POS;
				//				}
				break;
			}
			case CHECKING_FOR_GRIPPER_CONNECTION:{
				if(robotIndex_rx == (robot_index +1)){
					targetInOrganism = true;
				}
				break;
			}
			case CHECKING_FOR_ORGANISM_READY:{
				organismReady = true;
				break;
			}
			case ORGANISM_HORIZONTAL_MOVE:{
				detach = true;
				break;
			}
			case DETACH:{
				if(robotIndex_rx == total_robot_number){
					way_point = gazebo::math::Vector3(packet->position[0],packet->position[1],packet->position[2]- (robot_index-1)*GRIPPER_CONNECTION_DISTANCE);

				}
				break;
			}
			default:{
			}
			}

			break;
		}
		default:{

		}
		}
	}

}

bool RobotDriver::checkForTrapped(int duration, float position_error, float desired_Vx, float desired_Wz){

	trapCount = Clampit(trapCount,-1,duration+1);

	if(position_error > (POSITION_ERROR_THRESHOLD*2)){
		if((robotState.Vx<desired_Vx*0.4)||(robotState.Wz<desired_Wz*0.4)){
			if((robotState.Vx <= POSITION_ERROR_THRESHOLD*0.4)&&(robotState.Wz<=0.05)){
				trapCount ++;
			}
		}else{
			trapCount -= 2;
		}
	}else{
		trapCount -= 2;
	}
	if(trapCount >= duration){
		return true;
	}
	return false;
}

void RobotDriver::calculateObstacleVectorFromLaser(float &x, float &y){
	float obsVecX = 0.0;
	float obsVecY = 0.0;
	if(laserData.size()==8){
		for(int i = 0;i<8;i++){
			float angle = (float)(i-4)*(PI/8.0f);
			float val = 0.13-(laserData[i] - 0.17);
			obsVecX += val*cos(angle);
			obsVecY += val*sin(angle);
		}
		if(fabs(obsVecX)>0.0001)
			x = obsVecX;
		else
			x = 0.0;
		if(fabs(obsVecY)>0.0001)
			y = obsVecY;
		else
			y = 0.0;
	}
}

float RobotDriver::GeneralizedLennardJones(float distance){
	if(distance == 0){
		return 0;
	}else{
		float targetDist = 0.5;
		float normDistExp = pow(targetDist/distance, 2);
		float gain = 0.1;
		float force = -gain/distance*(normDistExp*normDistExp - normDistExp);
		if(fabs(force)>0.5){
			force = copysign(0.5,force);
		}
		return force;
	}
}

void RobotDriver::cleaningUpdate(){

	/******************** STAGE 1 : GO TO STARTING POSITION *****************************/

	/* 1. Determine how many robots are needed in the team, in this simulation, we assume we will use 5 robots to begin with.
	 * They will broadcast their IDs to determine their "squad ID", which I will now assume it to be their robot_index.
	 *
	 * 2. From their squad ID(robot_index), go to the starting position.
	 *
	 * 3. Broadcast to neighbours "I am in position"
	 *
	 * 4. Seek to connect to a neighbour
	 *
	 */

	/******************** STAGE 2 : GO TO WAYPOINTS *****************************/

	/* 1. The cleaning linear speed is fixed
	 *
	 * 2. If robots want to make 90 degrees turn, detach, turn, attach
	 */


	switch(cleaning_init_step){
	case GO_TO_STARTING_POS:{
		//		std::cout<<"robot "<< robot_index<<std::endl;
		/* In this simulation, I will ignore the localization problem and assume they can use GPS, and the
		 * precision of the position is calibrated through local positioning.
		 *
		 * Packet loss is ignored too.
		 *
		 * This function will let the robot go to the starting position and connect to the next robot.
		 */

		// calculate starting position according to squad ID(robot_index), assume the robot is on the grey wall
		gazebo::math::Vector3 starting_position(-3.2,robotState.position.y ,2.9 - robot_index*GRIPPER_CONNECTION_DISTANCE);
		target_orientation = gazebo::math::Vector3(horizontalMoveDirection,robotState.orientation.y,0.0);
		if(setSpeedFromWorldPosition(starting_position,target_orientation,grey_wall_normal)){
			// broadcast reached position
			sendPackets(0.0,CLEANING,GO_TO_STARTING_POS);
			cleaning_init_step = (RecruitingSteps)(cleaning_init_step +1);
		}
		break;
	}
	case WAITING_FOR_TARGET_IN_POSITION:{
		//		gazebo::math::Vector3 starting_position(-3.2,robotState.position.y ,2.9 - robot_index*GRIPPER_CONNECTION_DISTANCE);
		//		gazebo::math::Vector3 target_orientation(1.0,robotState.orientation.y,0.0);
		//		if(setSpeedFromWorldPosition(starting_position,target_orientation,grey_wall_normal)!=1){
		//			cleaning_init_step = (RecruitingSteps)(cleaning_init_step -1);
		//		}
		printf("WAITING_FOR_TARGET_IN_POSITION \n");
		if(robot_index == total_robot_number){
			cleaning_init_step = (RecruitingSteps)(cleaning_init_step +1);
		}else if(targetInPosition){
			cleaning_init_step = (RecruitingSteps)(cleaning_init_step +1);
			targetInPosition = !targetInPosition;
		}
		break;
	}
	case CHECKING_FOR_GRIPPER_CONNECTION:{
		printf("CHECKING_FOR_GRIPPER_CONNECTION \n");
		if(robot_index != total_robot_number){
			if(!gripperTouched){
				arm1_angle-= 0.01;
				setGripperJointsPosition("arm1_joint", arm1_angle);
			}else{
				//sendPackets(0.0,CLEANING,WAITING_FOR_GRIPPER_CONNECTION);	// WAITING_FOR_GRIPPER_CONNECTION has just completed
				cleaning_init_step = (RecruitingSteps)(cleaning_init_step +1);
			}
		}else{
			//sendPackets(0.0,CLEANING,WAITING_FOR_GRIPPER_CONNECTION);	// WAITING_FOR_GRIPPER_CONNECTION has just completed
			cleaning_init_step = (RecruitingSteps)(cleaning_init_step +1);
		}
		break;
	}
	case CHECKING_FOR_ORGANISM_READY:{
		//		std::cout<<"CHECKING_FOR_ORGANISM_READY "<< std::endl;
		//		if(robot_index == total_robot_number){
		//			printf("detach = %s", detach?"true":"false");
		//		}
		printf("CHECKING_FOR_ORGANISM_READY \n");
		if((robot_index == total_robot_number)||targetInOrganism){
			sendPackets(0.0,CLEANING,CHECKING_FOR_GRIPPER_CONNECTION);	// this robot is ready for moving as an organism
			if(robot_index == 1){
				sendPackets(0.0,CLEANING,CHECKING_FOR_ORGANISM_READY);
				cleaning_init_step = (RecruitingSteps)(cleaning_init_step +1);
			}
		}
		if(organismReady){
			if(horizontalMove)
				cleaning_init_step = (RecruitingSteps)(cleaning_init_step +1);
			else
				cleaning_init_step = ORGANISM_VERTICAL_MOVE;
		}

		break;
	}
	case ORGANISM_HORIZONTAL_MOVE:{

		//		if(robot_index == total_robot_number){
		//			printf("detach = %s", detach?"true":"false");
		//		}
		setInOrganismSpeed(0.1);
		// move together in a slow speed
		base_cmd.linear.x = 0.1;
		base_cmd.angular.z = 0.0;

		// assign squad ID
		float obsX,obsY, obsAngle;
		if(laserData.size()==8){
			for(int i = 2;i<6;i++){
				float angle = (float)(i-4)*(PI/8.0f);
				float val = 0.13-(laserData[i] - 0.17);

				obsX += val*cos(angle);
				obsY += val*sin(angle);
			}

			if(fabs(obsX)<0.0001)
				obsX = 0.0;
			if(fabs(obsY)<0.0001)
				obsY = 0.0;
		}
		if((obsX!=0)||(obsY!=0)){
			obsAngle = atan2(obsY,obsX);
			//		std::cout<<"obsX = "<< obsX<<"obsY = "<< obsY<<"obsAngle = "<< obsAngle<< std::endl;
			if(fabs(obsAngle)<=20*PI/180){
				sendPackets(0.0,CLEANING,ORGANISM_HORIZONTAL_MOVE);
				//			base_cmd.linear.x = 0.0;
				detach = true;
			}
		}

		if(detach){
			base_cmd.linear.x = 0.0;
			if(robot_index != total_robot_number){
				setGripperJointsPosition("detach_gripper", 1);
			}
			cleaning_init_step = DETACH;
			horizontalMove = false;
			horizontalMoveDirection *= (-1.0);
		}
		break;
	}
	case ORGANISM_VERTICAL_MOVE:{
//		gazebo::math::Vector3 starting_position(-3.2,robotState.position.y ,2.9 - robot_index*GRIPPER_CONNECTION_DISTANCE);
		target_orientation = gazebo::math::Vector3(horizontalMoveDirection,robotState.orientation.y,0.0);
		if(setSpeedFromWorldPosition(way_point,target_orientation,grey_wall_normal)){
			// broadcast reached position
			sendPackets(0.0,CLEANING,GO_TO_STARTING_POS);
//			cleaning_init_step = (RecruitingSteps)(cleaning_init_step +1);
		}

		break;
	}
	case DETACH:{
		targetInOrganism = false;
		organismReady = false;
		if(arm1_angle<=0.0){
			arm1_angle+= 0.01;
			base_cmd.linear.x = 0.0;
			setGripperJointsPosition("arm1_joint", arm1_angle);
		}else{
			detach = false;
			gripperTouched = false;
			if(horizontalMove)
				target_orientation = gazebo::math::Vector3(horizontalMoveDirection,robotState.orientation.y,0.0);
			else
				target_orientation = gazebo::math::Vector3(0.0,robotState.orientation.y,-1.0);
			std::cout<<"rotating "<< std::endl;
			if(setSpeedFromWorldPosition(robotState.position,target_orientation,grey_wall_normal)){
				std::cout<<"rotated****"<<std::endl;
				//rotate gripper base
				if(base_revolute_angle<=PI/2){
					base_revolute_angle+= 0.01;
					base_cmd.linear.x = 0.0;
					setGripperJointsPosition("base_revolute_joint", base_revolute_angle);
				}else{
					sendPackets(0.0,CLEANING,DETACH);
					if(robot_index == total_robot_number)
						way_point = gazebo::math::Vector3(robotState.position.x,robotState.position.y,robotState.position.z - (robot_index-1)*GRIPPER_CONNECTION_DISTANCE);
					cleaning_init_step = ORGANISM_VERTICAL_MOVE;
				}
			}
		}
		break;
	}
	default:{
	}

	}
}

int RobotDriver::setInOrganismSpeed(float baseSpeed){
	for(int i = 0;i<neighbours.size();i++){
		//		current_orientation_quar.RotateVector(neighbours[i].relative_position);
		//		std::cout<<robot_index<<" || robot "<<neighbours[i].neighbour_index<<" position = "<<current_orientation_quar.RotateVector(neighbours[i].relative_position).x<<" "<<current_orientation_quar.RotateVector(neighbours[i].relative_position).y<<" "<<current_orientation_quar.RotateVector(neighbours[i].relative_position).z<<std::endl;
	}
	return 0;
}
