/*
 * Morphogen.h
 *
 *  Created on: 12 Jan 2016
 *      Author: sherry
 */

#ifndef MORPHOGEN_H_
#define MORPHOGEN_H_

class Morphogen {
public:
	Morphogen();
	virtual ~Morphogen();
};

#endif /* CLEANINGBOT_CONTROL_SRC_MORPHOGEN_H_ */
