/*
 * Hormone.h
 *
 *  Created on: 12 Jan 2016
 *      Author: sherry
 */

#ifndef HORMONE_H_
#define HORMONE_H_

#include <vector>

class Hormone {
public:
	Hormone();
	virtual ~Hormone();

	// rule chromosome
	struct RuleChromosome{
	int ruleType;
	int triggerType;
	};

	// hormone chromosome
	int hormoneID;
	int fixedDecayRate;
	int diffusionCoeff;
	int maxValue;	//value at which a saturation is reached
	int baseProductionRate;	// amount that is produced per time step without sensory stimulation

	std::vector<RuleChromosome> ruleChromosomes;




	// parameters
	int hormoneConcentration;
	int actuatorValue;
	int dependentDose;
	int fixedDose;
	int sensorValue;


	void hormoneConcentrationUpdate();
	static bool compareThreshold(int compareType, int value, int threshold);






};

#endif /* CLEANINGBOT_CONTROL_SRC_HORMONE_H_ */
