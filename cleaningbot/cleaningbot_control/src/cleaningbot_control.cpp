#include <iostream>
#include <stdio.h>
#include <ncurses.h>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/String.h>
#include <gazebo_msgs/GetModelState.h>
#include <gazebo_msgs/GetJointProperties.h>
#include "robotdriver.h"

/*
void modelStateCallback(const gazebo_msgs::ModelStates::ConstPtr& msg)
{
  ROS_INFO("I heard: [%s]", msg->name[0].c_str());
}
 */
int main(int argc, char** argv)
{
	//init the ROS node
	ros::init(argc, argv, "robot_driver");
	ros::NodeHandle n;
    int robot_number = 4;
	std::vector<RobotDriver*> drivers;

	for(int i = 1; i<(robot_number+1); i++){
		RobotDriver* driver = new RobotDriver(n, i);
		driver->total_robot_number = robot_number;
		drivers.push_back(driver);
	}

	// run loop
	while(n.ok()){
		for(int i = 0; i<robot_number; i++){
			drivers[i]->runLoop();
			ros::spinOnce();
		}
	}
//	nocbreak(); //return terminal to "cooked" mode
}
