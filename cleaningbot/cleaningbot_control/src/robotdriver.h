#ifndef ROBOTDRIVER_H
#define ROBOTDRIVER_H

#include <iostream>
#include <stdio.h>
#include <ncurses.h>
#include <cmath>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/String.h>
#include <gazebo_msgs/GetModelState.h>
#include <gazebo_msgs/GetJointProperties.h>
#include <sensor_msgs/LaserScan.h>
#include <gazebo/math/gzmath.hh>
//#include <gazebo/common/common.hh>
//#include "gazebo/common/PID.hh"
#include "cleaningbot_control/RSPacket.h"
#include "cleaningbot_control/JointPos.h"
#include "cleaningbot_control/ContactReading.h"

#define ROBOT_MAX_SPEED 0.3
#define ROBOT_MAX_ANGULAR_SPEED 0.4
#define PI 3.141592654
extern const char* broadcast_channel_name;
#define SENSING_RANGE 1.0
#define COMM_RANGE 30
#define POSITION_ERROR_THRESHOLD 0.02
#define GRIPPER_CONNECTION_DISTANCE 0.35

float Clampit(float it, float min, float max);
int Clampit(int it, int min, int max);

class RobotDriver
{
private:
	// robot state data
	struct robot_state{
		gazebo::math::Vector3 position;
		gazebo::math::Vector3 orientation;	// it's a direction vector! :-/
		float Vx;		// linear velocity x
		float Wz;		// angular velocity z
	}robotState;

	struct neighbours_map{
		int neighbour_index;
		gazebo::math::Vector3 relative_position;
	};

	enum RobotStatus{
		IDLE = 0,
		INIT,
		OBSTACLE,
		RECHARGING,
		FAULT,
		RESCUE,
		CONNECTIVITY,
		EXTRA_DIRTY,
		CLEANING,
		RECRUITING
	};

	enum RecruitingSteps{
		WAITING = 0,
		GO_TO_STARTING_POS,
		WAITING_FOR_TARGET_IN_POSITION,
		CHECKING_FOR_GRIPPER_CONNECTION,
		CHECKING_FOR_ORGANISM_READY,
		ORGANISM_HORIZONTAL_MOVE,
		ORGANISM_VERTICAL_MOVE,
		DETACH,
		RECRUITING_STEPS_COUNT
	};

	//! The node handle we'll be using
	ros::NodeHandle n_;
	//! We will be publishing to the "/base_controller/command" topic to issue commands
	ros::Publisher cmd_vel_pub_;
	ros::Publisher robot_comm_;
	ros::Publisher RS_broadcast_channel_pub_;
	ros::Publisher joint_controller_pub_;
	ros::Subscriber RS_broadcast_channel_sub_;
	ros::Subscriber contact_reading_sub_;
	ros::ServiceClient model_state_client_;
	//ros::ServiceClient joint_state_client_;
	ros::Subscriber sensor_laser_data_sub_;
	gazebo_msgs::GetModelState gms;
	gazebo_msgs::GetJointProperties gjp;
	geometry_msgs::Twist base_cmd;
	cleaningbot_control::RSPacket RS_send_packet;
	cleaningbot_control::JointPos JointControllerMsg;
	gazebo::math::Vector3 initial_orientation_vector;
	gazebo::math::Quaternion current_orientation_quar;
	gazebo::math::Vector3 target_orientation;
	void laserCallback(const sensor_msgs::LaserScan::ConstPtr& msg);
	void RSCallback( const cleaningbot_control::RSPacket::ConstPtr& packet);
	void contactCallback(const cleaningbot_control::ContactReading::ConstPtr& contactMsg);
	std::vector<float> laserData;
	std::vector<neighbours_map> neighbours;

	int robot_index;	// robot's unique ID, meaning the n number of robot that's been placed on the glass
	int squad_ID;		// robot's ID in organism
	std::vector<int> squad_numbers_required;

	// define grey wall normal vector
	gazebo::math::Vector3 grey_wall_normal;

	float arm1_joint_position;
	float arm2_joint_position;
	float base_revolute_joint_position;
	std::string arm1_joint_name;
	std::string arm2_joint_name;
	std::string base_revolute_joint_name;
	std::string gripee_name;
	bool trapped;
	int trapCount;
	RobotStatus robot_status;
	RecruitingSteps cleaning_init_step;

	bool gripperContact;

	gazebo::math::Vector3 way_point;

	// FOR TESTING
	bool next_target;
	float arm1_angle;
	float base_revolute_angle;
	bool targetInPosition;
	bool targetInOrganism;
	bool organismReady;
	bool detach;
	bool horizontalMove;
	bool gripperTouched;
	float horizontalMoveDirection;
	std::vector<int> readyToStartCleaning;


	// robot status update functions
	void cleaningUpdate();


public:
	int total_robot_number;
	RobotDriver(ros::NodeHandle &n, int robotIndex);
	int runLoop();
	void updateSensorData();
	void setActuators();
	void sendPackets(float hormone_levels, int robot_status, int data);
	void setGripperJointsPosition(std::string target_joint_name, float target_pos);
	int setSpeedFromWorldPosition(gazebo::math::Vector3& target_pos,gazebo::math::Vector3& target_orientation, gazebo::math::Vector3& normal);
	bool checkForTrapped(int duration, float position_error, float desired_Vx, float desired_Wz);
	void calculateObstacleVectorFromLaser(float &x, float &y);
	float GeneralizedLennardJones(float distance);
	int setInOrganismSpeed(float baseSpeed);
};

#endif
